import * as React from 'react';
import { range } from 'lodash';
import { FormatHelper } from 'adp-mdf-core';
import * as PropTypes from 'prop-types';
import { CompareOption, DateHelper } from './DateHelper';
import { isEqual } from 'lodash';

const YEAR_PAGE_SIZE = 12;

interface IWeekDays {
  sunday: boolean;
  monday: boolean;
  tuesday: boolean;
  wednesday: boolean;
  thursday: boolean;
  friday: boolean;
  saturday: boolean;
}

enum CalendarRenderingModes {
  date = 'date',
  year = 'year',
  month = 'month'
}

// This is the main interface to hold all settings
export interface ICalendarProps {
  // The current date to select
  selectedDate: Date;
  // Called when a date is clicked
  onChange: (selectedDate: Date, isValid?: boolean, validationMessage?: string, isoDate?: string) => void;
  // Called when the calendar rendering month is changed to allow the consuming application to reset the data
  onVisibleMonthChange?: (startDateOfTheMonth: Date) => void;
  // min date to select
  min?: Date;
  // max date to select
  max?: Date;
  // To suppress weekday selection
  blackoutWeekDays?: IWeekDays;
  // To set weekOffset
  weekOffset?: number;
  // To enable/disable the widget
  disabled?: boolean;
  // Custom dates to apply style to specific date. Also, these values are passed directly to the custom date renderer
  dates?: ICalendarDateRenderer[];
  // To apply custom class
  className?: string;
  // custom renderer for date
  dateRenderer?: any;
  // reset renderingMode to Date
  reset: boolean;
}

// Interface to track the widget state
interface ICalendarState {
  // selected date
  selectedDate?: Date;
  // date used to render the current layout
  renderingDate: Date;
  // year layout current page number
  currentYearPageNumber?: number;
  // current rendering mode
  renderMode: CalendarRenderingModes;
  // To supress weekday selection
  blackoutWeekDays?: number[];
  // min date to select
  min?: Date;
  // max date to select
  max?: Date;
  // Custom dates to apply style to specific date. Also, these values are passed directly to the custom date renderer
  dates?: ICalendarDateRenderer[];
  // To set weekOffset
  weekOffset?: number;
}

// Interface for each item in the widget
interface ICalendarItem<T> {
  // value of the item
  value: T;
  // the value is selected or not
  isSelected?: boolean;
  // to enable/diable the value selection
  isEnabled?: boolean;
  // event to handle when the value is selected
  onSelect?: (value: number | string | Date | any) => void;
}

// Interface for Date layout
interface ICalendarDateRenderer extends ICalendarItem<number> {
  // class name of the rendering date */
  className?: string;
  // additional data to be set by the consuming application, which can be used by the template */
  data?: any;
  date: Date;
}

// Interface for year layout
interface ICalendarYearRendererProps {
  // list of years to render */
  years: Array<ICalendarItemRender<number>>;
  // to enable/disable the previous button */
  hasPreviousPage: boolean;
  // to enable/disable the next button */
  hasNextPage: boolean;
}

// Interface for month layout
interface ICalendarItemRender<T> extends ICalendarItem<T> {
  // custom class name which will be passed by the consuming application*/
  className: string;
}

export class Calendar extends React.Component<ICalendarProps, ICalendarState> {
  static propTypes = {
    selectedDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
    onChange: PropTypes.func,
    onVisibleMonthChange: PropTypes.func,
    min: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
    max: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
    weekOffset: PropTypes.number,
    disabled: PropTypes.bool,
    className: PropTypes.string,
    dateRenderer: PropTypes.any,
    reset: PropTypes.bool,
  };

  dateRenderer: JSX.Element;

  // Initializing the widget
  constructor(props: ICalendarProps) {
    super(props);

    const selectedDate = this.initializeSelectedDate(props);

    const calendarState: ICalendarState = {
      renderingDate: DateHelper.getFirstDayOfTheMonth(selectedDate || DateHelper.today),
      renderMode: CalendarRenderingModes.date,
      selectedDate
    };

    calendarState.currentYearPageNumber = 0;

    this.state = this.setCalendarState(props, calendarState);

    // Getting custom renderer from the children
    if (props.dateRenderer) {
      this.dateRenderer = props.dateRenderer;
    }
  }

  initializeSelectedDate(props) {
    let selectedDate = null;

    if (props.selectedDate) {
      selectedDate = DateHelper.getDate(props.selectedDate);
    }

    return selectedDate;
  }

  // When new props are received, compare the value and render the component if required.
  componentWillReceiveProps(nextProps: ICalendarProps) {
    if (this.hasPropsChanged(nextProps)) {
      const newState = this.setCalendarState(nextProps, this.state);

      this.setState(newState);
    }
  }

  // This method shows the next/previous page layout based on the current rendering mode.
  private handleCalendarPage = (newPosition: number) => {
    if (this.state.renderMode === CalendarRenderingModes.year) {
      this.setState({ currentYearPageNumber: this.state.currentYearPageNumber + newPosition });
    }
    else {
      // get the 1st day of the rendering month
      const nextDate = DateHelper.getFirstDayOfTheMonth(this.state.renderingDate);
      // Change the rendering month based on the prev/next selection
      nextDate.setUTCMonth(nextDate.getUTCMonth() + newPosition);
      // update the rendering date
      this.setState({ renderingDate: nextDate });

      if (this.props.onVisibleMonthChange) {
        this.props.onVisibleMonthChange(nextDate);
      }
    }
  };

  private hasPropsChanged = (nextProps: ICalendarProps): boolean => {
    let hasPropsChanged = false;

    if (nextProps.blackoutWeekDays) {
      hasPropsChanged = isEqual(nextProps.blackoutWeekDays, this.state.blackoutWeekDays);
    }

    if (hasPropsChanged || this.state.blackoutWeekDays) {
      return true;
    }

    if (!hasPropsChanged && nextProps.dates) {
      hasPropsChanged = isEqual(nextProps.dates, this.state.dates);
    }

    if (hasPropsChanged || this.state.dates) {
      return true;
    }

    if (nextProps.selectedDate) {
      hasPropsChanged = !DateHelper.equals(DateHelper.getDate(nextProps.selectedDate), this.state.selectedDate);
    }

    if (hasPropsChanged || this.state.selectedDate) {
      return true;
    }

    if (nextProps.max) {
      hasPropsChanged = !DateHelper.equals(DateHelper.getDate(nextProps.max), this.state.max);
    }

    if (hasPropsChanged || this.state.max) {
      return true;
    }

    if (nextProps.min) {
      hasPropsChanged = !DateHelper.equals(DateHelper.getDate(nextProps.min), this.state.min);
    }

    if (hasPropsChanged || this.state.min) {
      return true;
    }

    if (nextProps.weekOffset) {
      hasPropsChanged = this.state.weekOffset !== nextProps.weekOffset;
    }

    if (nextProps.selectedDate) {
      hasPropsChanged = !DateHelper.equals(DateHelper.getDate(nextProps.selectedDate), this.state.selectedDate);
    }

    if (hasPropsChanged || this.state.selectedDate) {
      return true;
    }

    return hasPropsChanged;
  };

  private setCalendarState = (props: ICalendarProps, currentState: ICalendarState): ICalendarState => {
    // reset the renderingDate to selectedDate (if available) when popup is closed
    // but, on initial render, the selectedDate could be empty, so we need to use the renderingDate
    if (props.reset) {
      currentState.renderMode = CalendarRenderingModes.date;
      currentState.renderingDate = currentState.selectedDate || currentState.renderingDate;
    }

    // If blackout weekdays, convert the weekday in to its weekday Index.
    if (props.blackoutWeekDays) {
      const weekDays: IWeekDays = Object.assign({
        sunday: false,
        monday: false,
        tuesday: false,
        wednesday: false,
        thursday: false,
        friday: false,
        saturday: false
      }, props.blackoutWeekDays);

      const blackoutWeekDays: number[] = [];

      Object.keys(weekDays).map((item, index) => {
        if (props.blackoutWeekDays[item]) {
          blackoutWeekDays.push(index);
        }
      });

      currentState.blackoutWeekDays = blackoutWeekDays;
    }

    currentState.min = props.min ? DateHelper.getDate(props.min) : null;
    currentState.max = props.max ? DateHelper.getDate(props.max) : null;
    currentState.weekOffset = props.weekOffset || 0;

    if (props.selectedDate instanceof Date) {
      currentState.selectedDate = props.selectedDate;
    }
    else if (typeof props.selectedDate !== 'string') {
      // String format comes only at construction time, and has already been handled by the constructor.
      currentState.selectedDate = null;
    }

    // If custom dates are provided, then parse it to Date object
    if (props.dates) {
      currentState.dates = props.dates.map((item) => {
        item.date = DateHelper.getDate(item.date);
        return item;
      });
    }

    return currentState;
  };

  handleForward = () => this.handleCalendarPage(1);

  handleBackwards = () => this.handleCalendarPage(-1);

  showYearLayout = () => {
    this.setState({
      renderMode: CalendarRenderingModes.year,
      currentYearPageNumber: 0
    });
  };

  showMonthLayout = () => {
    this.setState({ renderMode: CalendarRenderingModes.month });
  };

  onMonthSelect = (month) => {
    const date = new Date(Date.UTC(this.state.renderingDate.getUTCFullYear(), month, this.state.renderingDate.getUTCDate()));

    this.setState({
      renderingDate: date,
      renderMode: CalendarRenderingModes.date
    });

    if (this.props.onVisibleMonthChange) {
      this.props.onVisibleMonthChange(date);
    }
  };

  onYearSelect = (year) => {
    const date = new Date(Date.UTC(year, this.state.renderingDate.getUTCMonth(), this.state.renderingDate.getUTCDate()));

    this.setState({
      renderingDate: date,
      renderMode: CalendarRenderingModes.date
    });

    if (this.props.onVisibleMonthChange) {
      this.props.onVisibleMonthChange(date);
    }
  };

  onChange = (selectedDate: Date) => {
    this.setState({ selectedDate });

    if (this.props.onChange) {
      this.props.onChange(selectedDate);
    }
  };

  render() {
    return (
      <div className={`calendar ${this.props.className || ''}`}>

        <CalendarHeaderRenderer
          calendarState={this.state}
          onBackClick={this.handleBackwards}
          onNextClick={this.handleForward}
          onMonthSelect={this.showMonthLayout}
          onYearSelect={this.showYearLayout}
        />

        {this.state.renderMode === CalendarRenderingModes.date &&
        <CalendarWeekRenderer weekOffSet={this.state.weekOffset} />
        }

        {this.state.renderMode === CalendarRenderingModes.date &&
        <CalendarDateRenderer
          renderingDate={this.state.renderingDate}
          selectedDate={this.state.selectedDate}
          weekOffset={this.props.weekOffset}
          onChange={this.onChange}
          dates={this.state.dates}
          max={this.state.max}
          min={this.state.min}
          blackoutWeekDays={this.state.blackoutWeekDays}
          dateRenderer={this.dateRenderer ? this.dateRenderer : CalendarDateItemRenderer} />
        }

        {this.state.renderMode === CalendarRenderingModes.month &&
        <CalendarMonthRenderer
          renderingDate={this.state.renderingDate}
          selectedDate={this.state.selectedDate}
          onChange={this.onMonthSelect}
          max={this.state.max}
          min={this.state.min}
        />
        }

        {this.state.renderMode === CalendarRenderingModes.year &&
        <CalendarYearRenderer
          renderingDate={this.state.renderingDate}
          selectedDate={this.state.selectedDate}
          onChange={this.onYearSelect}
          max={this.state.max}
          min={this.state.min}
          currentPage={this.state.currentYearPageNumber}
        />
        }
      </div>
    );
  }
}

// Component to render the header section
const CalendarHeaderRenderer = ({ calendarState, onBackClick, onNextClick, onMonthSelect, onYearSelect }) => {
  const hasBackButton = calendarState.renderMode !== CalendarRenderingModes.month;
  const hasNextButton = calendarState.renderMode !== CalendarRenderingModes.month;
  const monthName = FormatHelper.formatDate(calendarState.renderingDate, { month: 'short' });
  const year = calendarState.renderingDate.getUTCFullYear();

  return <div className="calendar-header">
    {hasBackButton &&
    <div className="calendar-header-navbutton" onClick={onBackClick}>
      <i className="fa fa-arrow-circle-left" />
    </div>
    }
    <div className="calendar-header-currentDate">
      <a className="calendar-header-currentDate-selector" onClick={onMonthSelect}>
        {monthName} <i className="fa fa-angle-down" />
      </a>
      <a className="calendar-header-currentDate-selector" onClick={onYearSelect}>
        {year} <i className="fa fa-angle-down" />
      </a>
    </div>
    {hasNextButton &&
    <div className="calendar-header-navbutton" onClick={onNextClick}>
      <i className="fa fa-arrow-circle-right" />
    </div>
    }
  </div>;
};

// Component to render the week names layout
const CalendarWeekRenderer = ({ weekOffSet }) => {
  const weekDays: Array<{ short: string, long: string }> = FormatHelper.getLocaleFormats().dateFormat.weeks;

  return <div className="calendar-weekdays">
    {range(weekOffSet, 7 + weekOffSet).map((index) => (
      <CalendarWeekItemRenderer weekName={weekDays[index % 7].short} key={`day-${index}`} />
    ))}
  </div>;
};

// Component to render each week name
const CalendarWeekItemRenderer = ({ weekName }) => (
  <div className="calendar-weekdays-item">
    {weekName}
  </div>
);

// Component to render date layout
const CalendarDateRenderer = ({ renderingDate, selectedDate, min, max, weekOffset, onChange, dates, blackoutWeekDays, dateRenderer }) => {
  const daysToRender = CalendarHelper.getRenderingDays(renderingDate, selectedDate, min, max, weekOffset, dates, blackoutWeekDays);

  return <div className="calendar-grid">
    {daysToRender.map((day, i) => (
      React.createElement(dateRenderer, {
        ...day,
        onSelect: (() => day.isEnabled ? onChange(day.date) : null),
        key: `day-${i}`
      })
    ))}
  </div>;
};

// Component to render each day
export const CalendarDateItemRenderer = (props: ICalendarDateRenderer) => (
  <div className={`calendar-grid-item ${props.className}`} onClick={(/* e */) => props.isEnabled ? props.onSelect(props.date) : null}>
    <div className="calendar-grid-item-day">
      {props.value}
    </div>
  </div>
);

const CalendarMonthRenderer = ({ renderingDate, selectedDate, min, max, onChange }) => {
  const monthsToRender = CalendarHelper.getRenderingMonths(renderingDate, selectedDate, min, max);

  return <div className="calendar-grid">
    {monthsToRender.map((month, i) => (
      <CalendarMonthItemRenderer  {...month} onSelect={() => onChange(i)} key={`month-${i}`} />
    ))}
  </div>;
};

const CalendarMonthItemRenderer = (props: ICalendarItemRender<string>) => (
  <div className="calendar-grid-select" onClick={props.onSelect}>
    <div className={`calendar-grid-select-item ${props.className || ''}`}>
      {props.value}
    </div>
  </div>
);

const CalendarYearRenderer = ({ renderingDate, currentPage, selectedDate, min, max, onChange }) => {
  const yearsToRender = CalendarHelper.getRenderingYears(renderingDate, currentPage, selectedDate, min, max);

  return <div className="calendar-grid">
    {yearsToRender.years.map((year, i) => (
      <CalendarYearItemRenderer  {...year} onSelect={() => onChange(year.value)} key={`year-${i}`} />
    ))}
  </div>;
};

const CalendarYearItemRenderer = (props: ICalendarItemRender<number>) => (
  <div onClick={() => props.isEnabled ? props.onSelect(props.value) : null} className="calendar-grid-select">
    <div className={`calendar-grid-select-item ${props.className || ''}`}>
      {props.value}
    </div>
  </div>
);

class CalendarHelper {
  static getRenderingDays = (renderingDate: Date, selectedDate: Date, min: Date, max: Date, weekOffset: number = 0, dates: ICalendarDateRenderer[], blackoutWeekDays: number[]): ICalendarDateRenderer[] => {
    const dateRange = CalendarHelper.getRenderingDateRange(renderingDate, weekOffset);

    let minSelectableDate = dateRange.monthStartDate;
    let maxSelectableDate = dateRange.monthEndDate;

    if (min && DateHelper.compare(min, dateRange.monthStartDate) === CompareOption.Greater) {
      minSelectableDate = min;
    }

    if (max && DateHelper.compare(max, dateRange.monthEndDate) === CompareOption.Less) {
      maxSelectableDate = max;
    }

    dates = dates || [];

    return dateRange.dates.map((item) => {
      let mappedRenderingDate: ICalendarDateRenderer = {
        value: item.getUTCDate(),
        date: item
      };

      mappedRenderingDate.isSelected = DateHelper.equals(item, selectedDate);
      mappedRenderingDate.isEnabled = DateHelper.isInRange(item, minSelectableDate, maxSelectableDate);

      if (blackoutWeekDays && mappedRenderingDate.isEnabled) {
        mappedRenderingDate.isEnabled = blackoutWeekDays.indexOf(item.getUTCDay()) === -1;
      }

      const customDate = dates.find((date) => DateHelper.equals(date.date, item));

      // If custom date is specified, set the value in the response so that the custom template can access the values
      if (customDate) {
        // transfer customDate props to template
        mappedRenderingDate = Object.assign({}, mappedRenderingDate, { ...customDate });
      }

      mappedRenderingDate.className = `${mappedRenderingDate.isSelected ? 'calendar-grid-item__selected' : mappedRenderingDate.isEnabled ? '' : 'calendar-grid-item__disabled'}`;

      if (DateHelper.isToday(item)) {
        mappedRenderingDate.className += ' calendar-grid-item__today';
      }

      return mappedRenderingDate;
    });
  };

  static getRenderingDateRange = (renderingDate: Date, weekOffset = 0): { start: Date, end: Date, monthStartDate: Date, monthEndDate: Date, dates: Date[] } => {
    // Get last month days difference
    const renderingMonthStartDate = DateHelper.getFirstDayOfTheMonth(renderingDate);

    // Calculate the prev months days based on weekOffSet
    let diff = renderingMonthStartDate.getUTCDay() - weekOffset;

    if (diff < 0) {
      diff += 7;
    }

    // Set the difference as startDate to render the Calendar
    const startDate = new Date(renderingMonthStartDate);
    startDate.setUTCDate((diff - 1) * -1);

    // Get current month last day of the month
    const renderingMonthEndDate = DateHelper.getLastDayOfTheMonth(renderingDate);
    const endDate = new Date(renderingMonthEndDate);

    // Calculate number of days been added in the Calender
    const daysAdded = diff + renderingMonthEndDate.getUTCDate();

    // Find if any additional date require from next month
    let daysToAdd = daysAdded % 7;

    // If any additional days is required, add them as endDate and move to next month
    if (daysToAdd > 0) {
      daysToAdd = 7 - daysToAdd;
      endDate.setUTCMonth(endDate.getUTCMonth() + 1);
      endDate.setUTCDate(daysToAdd);
    }

    const dateToBegin = new Date(startDate);
    dateToBegin.setUTCDate(dateToBegin.getUTCDate() - 1);

    return {
      start: startDate,
      end: endDate,
      monthStartDate: renderingMonthStartDate,
      monthEndDate: renderingMonthEndDate,
      dates: range(0, daysAdded + daysToAdd).map((/* index */) => DateHelper.getDate(new Date(dateToBegin.setUTCDate(dateToBegin.getUTCDate() + 1))))
    };
  };

  static getRenderingYears = (renderingDate: Date, currentPage: number, selectedDate?: Date, min?: Date, max?: Date): ICalendarYearRendererProps => {
    const selectedYear = selectedDate ? selectedDate.getUTCFullYear() : 0;
    // Calculating current rendering page details
    const yearPosition = renderingDate.getUTCFullYear() % YEAR_PAGE_SIZE;
    const pageMinYear = renderingDate.getUTCFullYear() + (currentPage * YEAR_PAGE_SIZE) - yearPosition;
    const pageMaxYear = pageMinYear + YEAR_PAGE_SIZE;

    const maxYear = max ? max.getUTCFullYear() : pageMaxYear + 1;
    const minYear = min ? min.getUTCFullYear() : pageMinYear - 1;

    const years = range(pageMinYear, pageMaxYear).map((index) => {
      const isEnabled = (index >= minYear && index <= maxYear);
      const isSelected = (index === selectedYear);

      return {
        value: index,
        isSelected: isSelected,
        isEnabled: isEnabled,
        className: ` ${isSelected ? 'calendar-grid-select-item__selected' : ''} ${!isEnabled ? 'calendar-grid-select-item__disabled' : ''}`
      };
    });

    return {
      years: years,
      hasNextPage: pageMaxYear > maxYear,
      hasPreviousPage: pageMinYear < minYear
    };
  };

  static getRenderingMonths = (renderingDate: Date, selectedDate?: Date, min?: Date, max?: Date): Array<ICalendarItemRender<string>> => {
    const currentDate = DateHelper.getFirstDayOfTheYear(renderingDate);
    const selectedMonth = (selectedDate ? selectedDate.getUTCMonth() : -1);

    max = max ? max : currentDate;
    min = min ? min : currentDate;

    min = DateHelper.getFirstDayOfTheMonth(min);

    /* This is a bad ass block comment that spans multiple
       lines. */
    return range(0, 12).map((index) => {
      // Get month names
      const monthName = FormatHelper.getLocaleFormats().dateFormat.months[currentDate.getUTCMonth()].short;

      // Check the date range validity
      const isEnabled = (currentDate.getTime() >= min.getTime() && currentDate.getTime() <= max.getTime());
      currentDate.setUTCMonth(currentDate.getUTCMonth() + 1);

      return {
        value: monthName,
        isEnabled: isEnabled,
        isSelected: index === selectedMonth,
        index: index,
        className: (isEnabled ? index === selectedMonth ? 'calendar-grid-select-item__selected' : '' : 'calendar-grid-select-item__disabled')
      };
    });
  };
}
