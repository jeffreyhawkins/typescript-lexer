export class Lexer {
  private readonly input: string;
  private position: number = 0;
  private readPosition: number = 0;
  private ch: string = '';

  constructor(input: string) {
    this.input = input;
    this.readChar();
  }

  readChar = ():void => {
    if (this.readPosition >= this.input.length) {
      this.ch = '';
    }
    else {
      this.ch = this.input[this.readPosition];
    }

    this.position = this.readPosition;
    this.readPosition += 1;
  };

  peakChar = ():string => {
    if (this.readPosition >= this.input.length) {
      return '';
    }
    else {
      return this.input[this.readPosition];
    }
  };

  getPosition = ():number => {
    return this.position;
  };

  getChar = ():string => {
    return this.ch;
  };

  getInput = (start: number, end: number):string => {
    return this.input.substring(start, end);
  }

}

