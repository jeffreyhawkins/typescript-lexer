import { Lexer } from './Lexer';

enum Tokens {
  ASSIGN = '=',
  ASTERISK = '*',
  BACK_QUOTE = '`',
  BACK_SLASH = '\\',
  BIT_AND = '&',
  BIT_OR = '|',
  COLON = ':',
  COMMA = ',',
  COMMENT = '//',
  COMMENT_BLOCK = '/*',
  DOLLAR_SIGN = '$',
  DOUBLE_EQUAL = '==',
  DOUBLE_QUOTE = '"',
  EOF = 'eof',
  EQUAL = '==',
  EQUAL_TRIPLE = '===',
  EXCLAMATION = '!',
  FORWARD_SLASH = '/',
  GREATER_THAN = '>',
  GREATER_THAN_EQUAL = '>=',
  ILLEGAL = 'illegal',
  LEFT_BRACE = '}',
  LEFT_BRACKET = '[',
  LEFT_PAREN = '(',
  LESS_THAN = '<',
  LESS_THAN_EQUAL = '<=',
  LINE_FEED = '/r',
  LOGICAL_AND = '&&',
  LOGICAL_OR = '||',
  MINUS = '-',
  NEW_LINE = '/n',
  NOT_EQUAL = '!=',
  NOT_EQUAL_TRIPLE = '!==',
  PERCENT_SIGN = '%',
  PERIOD = '.',
  PLUS = '+',
  QUESTION = '?',
  RIGHT_BRACE = '}',
  RIGHT_BRACKET = ']',
  RIGHT_PAREN = ')',
  SEMICOLON = ';',
  SINGLE_QUOTE = '\'',
  TAB = '\t'
}

export const ILLEGAL = 'illegal';
export const EOF = 'eof';

const IDENTIFIER = 'IDENTIFIER';
const KEYWORD = 'KEYWORD';
const NUMBER = 'NUMBER';

const IdentifierMap = [
  {
    literal: Tokens.LESS_THAN_EQUAL,
    token: 'LESS_THAN_EQUAL'
  },
  {
    literal: Tokens.GREATER_THAN_EQUAL,
    token: 'GREATER_THAN_EQUAL'
  },
  {
    literal: Tokens.LOGICAL_AND,
    token: 'LOGICAL_AND'
  },
  {
    literal: Tokens.LOGICAL_OR,
    token: 'LOGICAL_OR'
  },
  {
    literal: Tokens.COMMENT_BLOCK,
    token: 'COMMENT_BLOCK'
  },
  {
    literal: Tokens.COMMENT,
    token: 'COMMENT'
  },
  {
    literal: Tokens.EQUAL,
    token: 'EQUAL'
  },
  {
    literal: Tokens.NOT_EQUAL,
    token: 'NOT_EQUAL'
  },
  {
    literal: Tokens.NOT_EQUAL_TRIPLE,
    token: 'NOT_EQUAL_TRIPLE'
  },
  {
    literal: Tokens.EQUAL_TRIPLE,
    token: 'EQUAL_TRIPLE'
  },
  {
    literal: Tokens.TAB,
    token: 'TAB'
  },
  {
    literal: Tokens.NEW_LINE,
    token: 'NEW_LINE'
  },
  {
    literal: Tokens.LINE_FEED,
    token: 'LINE_FEED'
  },
  {
    literal: Tokens.COMMA,
    token: 'COMMA'
  },
  {
    literal: Tokens.SEMICOLON,
    token: 'SEMICOLON'
  },
  {
    literal: Tokens.LEFT_PAREN,
    token: 'LEFT_PAREN'
  },
  {
    literal: Tokens.RIGHT_PAREN,
    token: 'RIGHT_PAREN'
  },
  {
    literal: Tokens.RIGHT_BRACE,
    token: 'RIGHT_BRACE'
  },
  {
    literal: Tokens.LEFT_BRACE,
    token: 'LEFT_BRACE'
  },
  {
    literal: Tokens.ASSIGN,
    token: 'ASSIGN'
  },
  {
    literal: Tokens.ASTERISK,
    token: 'ASTERISK'
  },
  {
    literal: Tokens.COMMA,
    token: 'COMMA'
  },
  {
    literal: Tokens.SINGLE_QUOTE,
    token: 'SINGLE_QUOTE',
  },
  {
    literal: Tokens.DOUBLE_QUOTE,
    token: 'DOUBLE_QUOTE'
  },
  {
    literal: Tokens.MINUS,
    token: 'MINUS'
  },
  {
    literal: Tokens.PLUS,
    token: 'PLUS'
  },
  {
    literal: Tokens.PERIOD,
    token: 'PERIOD'
  },
  {
    literal: Tokens.FORWARD_SLASH,
    token: 'FORWARD_SLASH'
  },
  {
    literal: Tokens.COLON,
    token: 'COLON'
  },
  {
    literal: Tokens.QUESTION,
    token: 'QUESTION'
  },
  {
    literal: Tokens.GREATER_THAN,
    token: 'GREATER_THAN'
  },
  {
    literal: Tokens.LESS_THAN,
    token: 'LESS_THAN'
  },
  {
    literal: Tokens.LEFT_BRACKET,
    token: 'LEFT_BRACKET'
  },
  {
    literal: Tokens.RIGHT_BRACKET,
    token: 'RIGHT_BRACKET'
  },
  {
    literal: Tokens.BIT_OR,
    token: 'BIT_OR'
  },
  {
    literal: Tokens.BIT_AND,
    token: 'BIT_AND'
  },
  {
    literal: Tokens.EXCLAMATION,
    token: 'EXCLAMATION'
  },
  {
    literal: Tokens.BACK_QUOTE,
    token: 'BACK_QUOTE'
  },
  {
    literal: Tokens.DOLLAR_SIGN,
    token: 'DOLLAR_SIGN'
  },
  {
    literal: Tokens.PERCENT_SIGN,
    token: 'PERCENT_SIGN'
  }
];

const Keywords = [
  'any',
  'as',
  'async',
  'await',
  'boolean',
  'break',
  'catch',
  'class',
  'const',
  'constructor',
  'continue',
  'debugger',
  'declare',
  'default',
  'delete',
  'do',
  'else',
  'enum',
  'export',
  'export',
  'extends',
  'false',
  'finally',
  'for',
  'from',
  'from',
  'function',
  'get',
  'if',
  'implements',
  'import',
  'in',
  'instanceof',
  'interface',
  'let',
  'module',
  'namespace',
  'new',
  'null',
  'number',
  'of',
  'package',
  'private',
  'protected',
  'public',
  'require',
  'return',
  'set',
  'static',
  'string',
  'super',
  'switch',
  'symbol',
  'this',
  'throw',
  'true',
  'try',
  'type',
  'typeof',
  'undefined',
  'var',
  'void',
  'while',
  'with',
  'yield'
];

export interface IToken {
  type: string;
  literal: string;
}

export const nextToken = (lexer: Lexer):IToken => {
  let tok: IToken = { type: ILLEGAL, literal: '' };

  skipWhiteSpace(lexer);

  switch (lexer.getChar()) {

    case '=':
      if (lexer.peakChar() === '=') {
        lexer.readChar();

        if (lexer.peakChar() === '=') {
          lexer.readChar();
          tok = newToken(Tokens.EQUAL_TRIPLE, "===");
        }
        else {
          tok = newToken(Tokens.EQUAL, "==");
        }
      }
      else {
        tok = newToken(Tokens.ASSIGN, lexer.getChar());
      }
      break;

    case '!':
      if (lexer.peakChar() === '=') {
        lexer.readChar();

        if (lexer.peakChar() === '=') {
          lexer.readChar();
          tok = newToken(Tokens.NOT_EQUAL_TRIPLE, "!==");
        }
        else {
          tok = newToken(Tokens.NOT_EQUAL, "!=");
        }
      }
      else {
        tok = newToken(Tokens.EXCLAMATION, lexer.getChar());
      }
      break;

    case ';':
      tok = newToken(Tokens.SEMICOLON, lexer.getChar());
      break;

    case '(':
      tok = newToken(Tokens.LEFT_PAREN, lexer.getChar());
      break;

    case ')':
      tok = newToken(Tokens.RIGHT_PAREN, lexer.getChar());
      break;

    case ',':
      tok = newToken(Tokens.COMMA, lexer.getChar());
      break;

    case '':
      tok = newToken(Tokens.EOF, '');
      break;

    case '*':
      tok = newToken(Tokens.ASTERISK, lexer.getChar());
      break;

    case '\'':
      tok = newToken(Tokens.SINGLE_QUOTE, lexer.getChar());
      break;

    case '{':
      tok = newToken(Tokens.LEFT_BRACE, lexer.getChar());
      break;

    case '}':
      tok = newToken(Tokens.RIGHT_BRACE, lexer.getChar());
      break;

    case '-':
      tok = newToken(Tokens.MINUS, lexer.getChar());
      break;

    case '+':
      tok = newToken(Tokens.PLUS, lexer.getChar());
      break;

    case '.':
      tok = newToken(Tokens.PERIOD, lexer.getChar());
      break;

    case '/':
      if (lexer.peakChar() === '/') {
        lexer.readChar();
        lexer.readChar();

        skipWhiteSpace(lexer);

        const comment = readLineComment(lexer);
        tok = newToken(Tokens.COMMENT, comment);
      }
      else if (lexer.peakChar() === '*') {
        lexer.readChar();
        lexer.readChar();
        skipWhiteSpace(lexer);

        const comment = readBlockComment(lexer);
        tok = newToken(Tokens.COMMENT_BLOCK, comment);
      }
      else {
        tok = newToken(Tokens.FORWARD_SLASH, lexer.getChar());
      }
      break;

    case ':':
      tok = newToken(Tokens.COLON, lexer.getChar());
      break;

    case '?':
      tok = newToken(Tokens.QUESTION, lexer.getChar());
      break;

    case '>':
      if (lexer.peakChar() === '=') {
        lexer.readChar();
        tok = newToken(Tokens.GREATER_THAN_EQUAL, '>=');
      }
      else {
        tok = newToken(Tokens.GREATER_THAN, lexer.getChar());
      }
      break;

    case '<':
      if (lexer.peakChar() === '=') {
        lexer.readChar();
        tok = newToken(Tokens.LESS_THAN_EQUAL, '<=');
      }
      else {
        tok = newToken(Tokens.LESS_THAN, lexer.getChar());
      }
      break;

    case '[':
      tok = newToken(Tokens.LEFT_BRACKET, lexer.getChar());
      break;

    case ']':
      tok = newToken(Tokens.RIGHT_BRACKET, lexer.getChar());
      break;

    case '|':
      if (lexer.peakChar() === '|') {
        lexer.readChar();
        tok = newToken(Tokens.LOGICAL_OR, '||');
      }
      else {
        tok = newToken(Tokens.BIT_OR, lexer.getChar());
      }
      break;

    case '&':
      if (lexer.peakChar() === '&') {
        lexer.readChar();
        tok = newToken(Tokens.LOGICAL_AND, '&&');
      }
      else {
        tok = newToken(Tokens.BIT_AND, lexer.getChar());
      }
      break;

    case '`':
      tok = newToken(Tokens.BACK_QUOTE, lexer.getChar());
      break;

    case '$':
      tok = newToken(Tokens.DOLLAR_SIGN, lexer.getChar());
      break;

    case '"':
      tok = newToken(Tokens.DOUBLE_QUOTE, lexer.getChar());
      break;

    case '%':
      tok = newToken(Tokens.PERCENT_SIGN, lexer.getChar());
      break;

    case '\n':
      tok = newToken(Tokens.NEW_LINE, lexer.getChar());
      break;

    case '\r':
      tok = newToken(Tokens.LINE_FEED, lexer.getChar());
      break;

    case '\t':
      tok = newToken(Tokens.TAB, lexer.getChar());
      break;

    default: {
      if (isLetter(lexer.getChar())) {
        let type = IDENTIFIER;
        const identifier = readIdentifier(lexer);
        if (isKeyword(identifier)) {
          type = KEYWORD;
        }

        return {
          type: type,
          literal: identifier
        };
      }
      else if (isDigit(lexer.getChar())) {
        return {
          type: NUMBER,
          literal: readNumber(lexer)
        }
      }
      else {
        tok = newToken(Tokens.ILLEGAL, lexer.getChar());
      }
    }
  }

  lexer.readChar();

  return tok;
};

const readIdentifier = (lexer: Lexer):string => {
  let position: number = lexer.getPosition();

  while (isLetter(lexer.getChar())) {
    lexer.readChar();
  }

  return lexer.getInput(position, lexer.getPosition());
};

const readNumber = (lexer: Lexer):string => {
  const position: number = lexer.getPosition();

  while (isDigit(lexer.getChar())) {
    lexer.readChar();
  }

  return lexer.getInput(position, lexer.getPosition());
};

const readLineComment = (lexer: Lexer):string => {
  const position: number = lexer.getPosition();

  while (lexer.getChar() !== '\n' && lexer.getChar() !== '\r') {
    lexer.readChar();
  }

  return lexer.getInput(position, lexer.getPosition());
};

const readBlockComment = (lexer: Lexer):string => {
  const position: number = lexer.getPosition();

  while (lexer.getChar() !== '*' && lexer.peakChar() !== '/') {
    lexer.readChar();
  }

  return lexer.getInput(position, lexer.getPosition());
};

const isLetter = (ch: string): boolean => {
  return 'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z' || ch == '_';
};

const newToken = (type: Tokens, ch: string):IToken => {
  return {
    type: lookupIdentifier(type),
    literal: ch
  };
};

const skipWhiteSpace = (lexer: Lexer):void => {
  while (lexer.getChar() === ' ' || lexer.getChar() === '\t' || lexer.getChar() === '\n' || lexer.getChar() === '\r') {
    lexer.readChar();
  }
};

const isDigit = (ch: string):boolean => {
  return '0' <= ch && ch <= '9';
};

const lookupIdentifier = (literal: Tokens):string => {
  for (let i = 0; i < IdentifierMap.length; i++) {
    const ident = IdentifierMap[i];
    if (ident.literal === literal) {
      return ident.token;
    }
  }

  return literal;
};

const isKeyword = (literal: string):boolean => {
  for (const k of Keywords) {
    if (literal === k) {
      return true;
    }
  }

  return false;
};