import * as fs from 'fs';
import { Lexer } from './Lexer';
import { EOF, ILLEGAL, IToken, nextToken} from './Tokens';

const main = () => {
  const buffer = fs.readFileSync('./samples/Calendar.tsx');
  const code = buffer.toString('utf8');

  const tokens = [];
  const lexer: Lexer = new Lexer(code);

  while (true) {
    const tok: IToken = nextToken(lexer);

    console.log('token: ' + JSON.stringify(tok));

    if (tok.type === ILLEGAL) {
      console.log('ILLEGAL TOKEN AT: ' + JSON.stringify(tok));
      break;
    }
    else if (tok.type === EOF) {
      console.log('done');
      break;
    }
    else {
      tokens.push(tok);
    }
  }

  console.log('token count: ' + tokens.length);
};

main();